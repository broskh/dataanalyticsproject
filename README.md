# DataAnalyticsProject

Project for Data Analytics exam

## Data visualizator

To run the Django project to visualize and explore data:

```
cd data_analytics_visualization/
source venv/bin/activate
venv/bin/python manage.py runserver
```

Then open the [page](http://127.0.0.1:8000/)

## Links

colab project link: [Colab](hhttps://colab.research.google.com/drive/1Z7g-qKa4Xu8U_Rv_6j5-6I3K6WQX_yjz?usp=sharing)

drive link: [Drive folder](https://drive.google.com/drive/folders/1Z8CIZdAXMEAxJpom3nffrEhzcrcQfvdO?usp=sharing)

visualizator link: [Django data visualizator](https://drive.google.com/file/d/1BQt8b0QXXAwEjS35fhS2Iz-y8HvXDyq0/view?usp=sharing)