from django.conf.urls import url
from django.contrib import admin
from django.urls import include

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include(('results.urls', 'results'), namespace="results")),
    url(r'^tools/', include(('tools.urls', 'tools'), namespace="tools")),
]