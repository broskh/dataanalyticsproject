import pickle

import nltk
# nltk.download('stopwords')
# nltk.download('punkt')
# nltk.download('wordnet')
# nltk.download('averaged_perceptron_tagger')
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet

from afinn import Afinn
from dandelion import DataTXT
from spellchecker import SpellChecker
import spacy

import re
from collections import Counter
import itertools

from results.models import EntityRelation


dandelion_token = '76fc40d971fb4401aa5c3f5e5cc86a87'
datatxt = DataTXT(token=dandelion_token)
nlp = spacy.load("en_core_web_sm")

html_pattern = re.compile('<.*?>')
url_pattern = re.compile(r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+')
emoji_pattern = re.compile(r"<U\+.{8}>")
with open('data/slang.txt') as file:
    chat_words_str = file.read()
chat_words_map_dict = {}
chat_words_list = []
for line in chat_words_str.split("\n"):
    if line != "":
        cw = line.split("=")[0]
        cw_expanded = line.split("=")[1]
        chat_words_list.append(cw)
        chat_words_map_dict[cw] = cw_expanded
chat_words_list = set(chat_words_list)
spell = SpellChecker()

other_stop = set()
# adding some of the stopwords after observing the tweets
other_stop.add("rt")
other_stop.add("…")
other_stop.add("’")
other_stop.add("..")
other_stop.add("•")
other_stop.add("“")
other_stop.add("”")
other_stop.add("—")
other_stop.add("...")
other_stop.add("‘")
other_stop.add("©")
other_stop.add(". .")
other_stop.add("–")

lmt = WordNetLemmatizer()
wordnet_map = {"N": wordnet.NOUN, "V": wordnet.VERB, "J": wordnet.ADJ, "R": wordnet.ADV}

afinn = Afinn(emoticons=True)

word_features = []

# open file and read the content in a list
with open('data/features.txt', 'r') as filehandle:
    for line in filehandle:
        # remove linebreak which is the last character of the string
        currentPlace = line[:-1]

        # add item to the list
        word_features.append(currentPlace)

classifier = pickle.load(open('data/model_nbc.sav', 'rb'))


def remove_html(text):
    return html_pattern.sub(r'', text)


def remove_urls(text):
    return url_pattern.sub(r'', text)


def chat_words_conversion(text):
    new_text = []
    for w in text.split():
        if w.upper() in chat_words_list:
            new_text.append(chat_words_map_dict[w.upper()])
        else:
            new_text.append(w)
    return " ".join(new_text)


def correct_spellings(text):
    corrected_text = []
    misspelled_words = spell.unknown(text.split())
    for word in text.split():
        if word in misspelled_words:
            corrected_text.append(spell.correction(word))
        else:
            corrected_text.append(word)
    return " ".join(corrected_text)


def encode_emoji(text):
    for match in emoji_pattern.findall(text):
        text = text.replace(match, f"\\U{match[3:-1]}".encode().decode('unicode-escape'))
    return text


def get_counter(df):
    sentences = (list(itertools.chain(df)))
    flat_list = [item for sublist in sentences for item in sublist]
    c = Counter(flat_list)
    return c


def get_ents_from_tokens(tokens):
    return nlp(" ".join(tokens))


# def get_ent_text(ents):
#   return [str(item.text) for item in ents.ents]

def get_people(ents):
    return [item.text.lower() for item in ents.ents if item.label_ == 'PERSON']


def get_not_people(ents):
    return [item.text.lower() for item in ents.ents if item.label_ != 'PERSON']


def get_nlp_tweet_to_render(tweet, entities):
    ents = []
    items = []
    for ent in entities.ents:
        found = False
        for item in items:
            if item.label_ == ent.label_ and item.text == ent.text:
                found = True
        if not found:
            items.append(ent)
    for item in items:
        for match in re.finditer(item.text, tweet):
            new_ent = {
                "start": match.start(),
                "end": match.end(),
                "label": item.label_
            }
            if len(ents) == 0:
                ents.append(new_ent)
            else:
                y = range(new_ent["start"], new_ent["end"])
                found = False
                for ent in ents:
                    x = range(ent["start"], ent["end"])
                    xs = set(x)
                    if len(xs.intersection(y)) > 0:
                        if len(y) > len(x):
                            ents.remove(ent)
                        else:
                            found = True
                if not found:
                    ents.append(new_ent)
    ents = sorted(ents, key=lambda i: i['start'])
    return {
        "text": tweet,
        "ents": ents
    }


def get_nlp_tweet_to_render_django(tweet):
    ents = []
    items = []
    for rel in EntityRelation.objects.filter(tweet=tweet):
        ent = rel.entity
        found = False
        for item in items:
            if item.label == ent.label and item.entity == ent.entity:
                found = True
        if not found:
            items.append(ent)
    for item in items:
        for match in re.finditer(item.entity, tweet.text.lower()):
            new_ent = {
                "start": match.start(),
                "end": match.end(),
                "label": item.label
            }
            if len(ents) == 0:
                ents.append(new_ent)
            else:
                y = range(new_ent["start"], new_ent["end"])
                found = False
                for ent in ents:
                    x = range(ent["start"], ent["end"])
                    xs = set(x)
                    if len(xs.intersection(y)) > 0:
                        if len(y) > len(x):
                            ents.remove(ent)
                        else:
                            found = True
                if not found:
                    ents.append(new_ent)
    ents = sorted(ents, key=lambda i: i['start'])
    return {
        "text": tweet.text,
        "ents": ents
    }


def lemmatize_words(tokens):
    pos_tagged_text = nltk.pos_tag(tokens)
    return [lmt.lemmatize(word, wordnet_map.get(pos[0], wordnet.NOUN)) for word, pos in pos_tagged_text]


def extract_features(document):
    document_words = set(document)
    features = {}
    for word in word_features:
        features['contains(%s)' % word] = (word in document_words)
    return features
