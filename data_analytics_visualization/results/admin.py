from django.contrib import admin

from results.models import SimpleToken, TweetToken, PuncToken, Tweet, LancasterStem, PorterStem, SnowballStem, \
    LemmatizerStem, StopwordToken, Entity, SimpleTokenRelation, PuncTokenRelation, TweetTokenRelation, \
    StopwordTokenRelation, LancasterStemRelation, PorterStemRelation, SnowballStemRelation, LemmatizerStemRelation, \
    EntityRelation


class SimpleTokenAdmin(admin.ModelAdmin):
    fields = ['token']
    # list_display = ('token')
    # list_filter = ['token']
    # search_fields = ['token']


class SimpleTokenRelationAdmin(admin.ModelAdmin):
    fields = ['token', 'tweet']
    list_display = ['token', 'tweet', 'counter']


class PuncTokenAdmin(admin.ModelAdmin):
    fields = ['token']
    # list_display = ('token')
    # list_filter = ['token']
    # search_fields = ['token']


class PuncTokenRelationAdmin(admin.ModelAdmin):
    fields = ['token', 'tweet']
    list_display = ['token', 'tweet', 'counter']


class TweetTokenAdmin(admin.ModelAdmin):
    fields = ['token']
    # list_display = ('token')
    # list_filter = ['token']
    # search_fields = ['token']


class TweetTokenRelationAdmin(admin.ModelAdmin):
    fields = ['token', 'tweet']
    list_display = ['token', 'tweet', 'counter']


class StopwordTokenAdmin(admin.ModelAdmin):
    fields = ['token']
    # list_display = ('token')
    # list_filter = ['token']
    # search_fields = ['token']


class StopwordTokenRelationAdmin(admin.ModelAdmin):
    fields = ['token', 'tweet']
    list_display = ['token', 'tweet', 'counter']


class LancasterStemAdmin(admin.ModelAdmin):
    fields = ['token']
    # list_display = ('token')
    # list_filter = ['token']
    # search_fields = ['token']


class LancasterStemRelationAdmin(admin.ModelAdmin):
    fields = ['token', 'tweet']
    list_display = ['token', 'tweet', 'counter']


class PorterStemAdmin(admin.ModelAdmin):
    fields = ['token']
    # list_display = ('token')
    # list_filter = ['token']
    # search_fields = ['token']


class PorterStemRelationAdmin(admin.ModelAdmin):
    fields = ['token', 'tweet']
    list_display = ['token', 'tweet', 'counter']


class SnowballStemAdmin(admin.ModelAdmin):
    fields = ['token']
    # list_display = ('token')
    # list_filter = ['token']
    # search_fields = ['token']


class SnowballStemRelationAdmin(admin.ModelAdmin):
    fields = ['token', 'tweet']
    list_display = ['token', 'tweet', 'counter']


class LemmatizerStemAdmin(admin.ModelAdmin):
    fields = ['token']
    # list_display = ('token')
    # list_filter = ['token']
    # search_fields = ['token']


class LemmatizerStemRelationAdmin(admin.ModelAdmin):
    fields = ['token', 'tweet']
    list_display = ['token', 'tweet', 'counter']


class EntityAdmin(admin.ModelAdmin):
    fields = ['entity', 'label']
    # list_display = ('token')
    # list_filter = ['entity', 'label']
    # search_fields = ['entity', 'label']


class EntityRelationAdmin(admin.ModelAdmin):
    fields = ['entity', 'tweet']
    list_display = ['entity', 'tweet', 'counter']


class TweetAdmin(admin.ModelAdmin):
    fields = ['index', 'text', 'text_cleaned', 'afinn', 'sentiment140']
    # fields = ['index', 'text', 'text_cleaned', 'punc_tokens', 'simple_tokens', 'tweet_tokens', 'stopword_tokens',
    #           'lancaster_stems', 'porter_stems', 'snowball_stems', 'lemmatizer_stems', 'afinn', 'sentiment140',
    #                 'entities']
    list_display = ('index', 'text')
    # list_filter = ['index', 'text', 'text_cleaned', 'punc_tokens', 'simple_tokens', 'tweet_tokens', 'stopword_tokens',
    #           'lancaster_stems', 'porter_stems', 'snowball_stems', 'lemmatizer_stems', 'afinn', 'sentiment140',
    #                 'entities']
    # search_fields = ['index', 'text', 'text_cleaned', 'punc_tokens', 'simple_tokens', 'tweet_tokens', 'stopword_tokens',
    #           'lancaster_stems', 'porter_stems', 'snowball_stems', 'lemmatizer_stems', 'afinn', 'sentiment140',
    #                 'entities']


admin.site.register(SimpleToken, SimpleTokenAdmin)
admin.site.register(SimpleTokenRelation, SimpleTokenRelationAdmin)
admin.site.register(PuncToken, PuncTokenAdmin)
admin.site.register(PuncTokenRelation, PuncTokenRelationAdmin)
admin.site.register(TweetToken, TweetTokenAdmin)
admin.site.register(TweetTokenRelation, TweetTokenRelationAdmin)
admin.site.register(StopwordToken, StopwordTokenAdmin)
admin.site.register(StopwordTokenRelation, StopwordTokenRelationAdmin)
admin.site.register(LancasterStem, LancasterStemAdmin)
admin.site.register(LancasterStemRelation, LancasterStemRelationAdmin)
admin.site.register(PorterStem, PorterStemAdmin)
admin.site.register(PorterStemRelation, PorterStemRelationAdmin)
admin.site.register(SnowballStem, SnowballStemAdmin)
admin.site.register(SnowballStemRelation, SnowballStemRelationAdmin)
admin.site.register(LemmatizerStem, LemmatizerStemAdmin)
admin.site.register(LemmatizerStemRelation, LemmatizerStemRelationAdmin)
admin.site.register(Entity, EntityAdmin)
admin.site.register(EntityRelation, EntityRelationAdmin)
admin.site.register(Tweet, TweetAdmin)
