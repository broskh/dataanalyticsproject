from django.db import models


class PuncToken(models.Model):
    token = models.TextField()

    def __str__(self):
        return self.token


class SimpleToken(models.Model):
    token = models.TextField()

    def __str__(self):
        return self.token


class TweetToken(models.Model):
    token = models.TextField()

    def __str__(self):
        return self.token


class StopwordToken(models.Model):
    token = models.TextField()

    def __str__(self):
        return self.token


class LancasterStem(models.Model):
    token = models.TextField()

    def __str__(self):
        return self.token


class PorterStem(models.Model):
    token = models.TextField()

    def __str__(self):
        return self.token


class SnowballStem(models.Model):
    token = models.TextField()

    def __str__(self):
        return self.token


class LemmatizerStem(models.Model):
    token = models.TextField()

    def __str__(self):
        return self.token


class Entity(models.Model):
    entity = models.TextField()
    label = models.TextField()

    def __str__(self):
        return self.entity


class Tweet(models.Model):

    index = models.PositiveIntegerField()
    text = models.TextField()
    text_cleaned = models.TextField()
    punc_tokens = models.ManyToManyField(PuncToken, through="PuncTokenRelation")
    simple_tokens = models.ManyToManyField(SimpleToken, through="SimpleTokenRelation")
    tweet_tokens = models.ManyToManyField(TweetToken, through="TweetTokenRelation")
    stopword_tokens = models.ManyToManyField(StopwordToken, through="StopwordTokenRelation")
    lancaster_stems = models.ManyToManyField(LancasterStem, through="LancasterStemRelation")
    porter_stems = models.ManyToManyField(PorterStem, through="PorterStemRelation")
    snowball_stems = models.ManyToManyField(SnowballStem, through="SnowballStemRelation")
    lemmatizer_stems = models.ManyToManyField(LemmatizerStem, through="LemmatizerStemRelation")
    afinn = models.IntegerField()
    sentiment140 = models.PositiveIntegerField()
    entities = models.ManyToManyField(Entity, through="EntityRelation")

    class Meta:
        index_together = [['index']]

    def __str__(self):
        return self.text


class PuncTokenRelation(models.Model):
    token = models.ForeignKey(PuncToken, on_delete=models.CASCADE)
    tweet = models.ForeignKey(Tweet, on_delete=models.CASCADE)
    counter = models.PositiveIntegerField(default=1)


class SimpleTokenRelation(models.Model):
    token = models.ForeignKey(SimpleToken, on_delete=models.CASCADE)
    tweet = models.ForeignKey(Tweet, on_delete=models.CASCADE)
    counter = models.PositiveIntegerField(default=1)


class TweetTokenRelation(models.Model):
    token = models.ForeignKey(TweetToken, on_delete=models.CASCADE)
    tweet = models.ForeignKey(Tweet, on_delete=models.CASCADE)
    counter = models.PositiveIntegerField(default=1)


class StopwordTokenRelation(models.Model):
    token = models.ForeignKey(StopwordToken, on_delete=models.CASCADE)
    tweet = models.ForeignKey(Tweet, on_delete=models.CASCADE)
    counter = models.PositiveIntegerField(default=1)


class LancasterStemRelation(models.Model):
    token = models.ForeignKey(LancasterStem, on_delete=models.CASCADE)
    tweet = models.ForeignKey(Tweet, on_delete=models.CASCADE)
    counter = models.PositiveIntegerField(default=1)


class PorterStemRelation(models.Model):
    token = models.ForeignKey(PorterStem, on_delete=models.CASCADE)
    tweet = models.ForeignKey(Tweet, on_delete=models.CASCADE)
    counter = models.PositiveIntegerField(default=1)


class SnowballStemRelation(models.Model):
    token = models.ForeignKey(SnowballStem, on_delete=models.CASCADE)
    tweet = models.ForeignKey(Tweet, on_delete=models.CASCADE)
    counter = models.PositiveIntegerField(default=1)


class LemmatizerStemRelation(models.Model):
    token = models.ForeignKey(LemmatizerStem, on_delete=models.CASCADE)
    tweet = models.ForeignKey(Tweet, on_delete=models.CASCADE)
    counter = models.PositiveIntegerField(default=1)


class EntityRelation(models.Model):
    entity = models.ForeignKey(Entity, on_delete=models.CASCADE)
    tweet = models.ForeignKey(Tweet, on_delete=models.CASCADE)
    counter = models.PositiveIntegerField(default=1)
