from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.ResultsView.as_view(), name='index'),
    url(r'^ajax/get-entity-details/$', views.get_entity_details, name='get_entity_details'),
    url(r'^complete-data$', views.CompleteDataView.as_view(), name='complete-data'),
    url(r'^complete-data-light$', views.CompleteDataLightView.as_view(), name='complete-data-light'),
    # url(r'^insert-data$', views.InsertDataView.as_view(), name='insert-data'),
]
