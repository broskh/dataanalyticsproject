from ast import literal_eval

from django.db.models import Sum
from django.http import JsonResponse
from django.views.generic import TemplateView

from spacy import displacy
import pandas as pd

from data_analytics_visualization.utils import get_nlp_tweet_to_render_django, datatxt, get_ents_from_tokens
from results.models import EntityRelation, PuncToken, PuncTokenRelation, SimpleToken, SimpleTokenRelation, TweetToken, \
    TweetTokenRelation, StopwordToken, StopwordTokenRelation, LancasterStem, LancasterStemRelation, PorterStem, \
    PorterStemRelation, SnowballStemRelation, SnowballStem, LemmatizerStemRelation

from results.models import Tweet, LemmatizerStem, Entity


class ResultsView(TemplateView):
    template_name = 'results/index.html'

    def get_context_data(self, **kwargs):
        context = super(ResultsView, self).get_context_data(**kwargs)
    
        negatives = Tweet.objects.filter(sentiment140=0)
        negatives_perc = negatives.count()/Tweet.objects.all().count()
        positives = Tweet.objects.filter(sentiment140=4)
        positives_perc = positives.count()/Tweet.objects.all().count()
        tokens = LemmatizerStem.objects.values('token')\
            .annotate(tweet_count=Sum('lemmatizerstemrelation__counter'))\
            .order_by("-tweet_count")
        positive_people = Entity.objects.values('id', 'entity')\
            .filter(entityrelation__tweet__in=positives, label="PERSON")\
            .annotate(entity_count=Sum('entityrelation__counter'))\
            .order_by("-entity_count")
        negative_people = Entity.objects.values('id', 'entity')\
            .filter(entityrelation__tweet__in=negatives, label="PERSON")\
            .annotate(entity_count=Sum('entityrelation__counter'))\
            .order_by("-entity_count")
        only_positive_people = Entity.objects.values('id', 'entity')\
            .filter(entityrelation__tweet__in=positives, label="PERSON")\
            .exclude(entity__in=negative_people.values('entity'))\
            .annotate(entity_count=Sum('entityrelation__counter'))\
            .order_by("-entity_count")
        only_negative_people = Entity.objects.values('id', 'entity')\
            .filter(entityrelation__tweet__in=negatives, label="PERSON")\
            .exclude(entity__in=positive_people.values('entity'))\
            .annotate(entity_count=Sum('entityrelation__counter'))\
            .order_by("-entity_count")
        positive_not_people = Entity.objects.values('id', 'entity')\
            .filter(entityrelation__tweet__in=positives)\
            .exclude(label="PERSON")\
            .annotate(entity_count=Sum('entityrelation__counter'))\
            .order_by("-entity_count")
        negative_not_people = Entity.objects.values('id', 'entity')\
            .filter(entityrelation__tweet__in=negatives)\
            .exclude(label="PERSON")\
            .annotate(entity_count=Sum('entityrelation__counter'))\
            .order_by("-entity_count")
        other_entities = Entity.objects.values('id', 'entity')\
            .exclude(label="PERSON")\
            .annotate(entity_count=Sum('entityrelation__counter'))\
            .order_by("-entity_count")

        context['webpage_title'] = 'Analisi esplorativa - Risultati'
        context['pc_labels'] = 'positivi,negativi'
        context['bc_labels'] = str(list(tokens[:10].values_list('token', flat=True)))\
            .replace("', '", ",").replace("['", "").replace("']", "")
        context['bc_data'] = str(list(tokens[:10].values_list('tweet_count', flat=True)))\
            .replace(", ", ",").replace("[", "").replace("]", "")
        context['bc_labels_ex_1'] = str(list(tokens[1:11].values_list('token', flat=True)))\
            .replace("', '", ",").replace("['", "").replace("']", "")
        context['bc_data_ex_1'] = str(list(tokens[1:11].values_list('tweet_count', flat=True)))\
            .replace(", ", ",").replace("[", "").replace("]", "")

        context['other_entities'] = other_entities.order_by("entity")
        context['positive_not_people'] = positive_not_people
        context['negative_not_people'] = negative_not_people
        context['positive_people'] = positive_people
        context['positive_people_sorted'] = positive_people.order_by("entity")
        context['only_positive_people'] = only_positive_people
        context['negative_people'] = negative_people
        context['negative_people_sorted'] = negative_people.order_by("entity")
        context['only_negative_people'] = only_negative_people
        context['tokens'] = tokens
        context['polarity'] = f'{positives_perc},{negatives_perc}'
        context['positives'] = positives
        context['negatives'] = negatives
        return context


def get_entity_details(request):
    entity_id = request.GET.get('entity_id', None)
    tweet = EntityRelation.objects.filter(entity_id=entity_id).first().tweet
    # noinspection PyUnresolvedReferences
    data = {
        'tweet': displacy.render(get_nlp_tweet_to_render_django(tweet),
                                 style='ent', page=True, manual=True),
        'nel': datatxt.nex(tweet.text).annotations
    }
    return JsonResponse(data)


class CompleteDataView(TemplateView):
    template_name = 'results/complete-data.html'

    def get_context_data(self, **kwargs):
        context = super(CompleteDataView, self).get_context_data(**kwargs)

        tweets = Tweet.objects.all().prefetch_related('punc_tokens', 'simple_tokens', 'tweet_tokens', 'stopword_tokens',
                                                    'lancaster_stems', 'porter_stems', 'snowball_stems',
                                                    'lemmatizer_stems', 'entities')

        context['webpage_title'] = 'Analisi esplorativa - Dati completi'

        context['tweets'] = tweets
        return context


class CompleteDataLightView(CompleteDataView):
    template_name = 'results/complete-data-light.html'

    def get_context_data(self, **kwargs):
        context = super(CompleteDataLightView, self).get_context_data(**kwargs)

        context['tweets'] = context['tweets'][:1000]
        return context


class InsertDataView(TemplateView):
    template_name = 'results/index.html'

    def get_context_data(self, **kwargs):
        context = super(InsertDataView, self).get_context_data(**kwargs)

        context['webpage_title'] = 'Analisi esplorativa - Inserimento Dati'


        filename = 'data/dataset_complete.csv'
        data = pd.read_csv(filename)
        data["text_cleaned_punc_tok"] = data["text_cleaned_punc_tok"].apply(literal_eval)
        data["text_cleaned_cleaned_stop"] = data["text_cleaned_stop"].apply(literal_eval)
        data["text_cleaned_stop_punc"] = data["text_cleaned_stop_punc"].apply(literal_eval)
        data["text_cleaned_lanc_stem"] = data["text_cleaned_lanc_stem"].apply(literal_eval)
        data["text_cleaned_port_stem"] = data["text_cleaned_port_stem"].apply(literal_eval)
        data["text_cleaned_snow_stem"] = data["text_cleaned_snow_stem"].apply(literal_eval)
        data["text_cleaned_simple_tok"] = data["text_cleaned_simple_tok"].apply(literal_eval)
        data["text_cleaned_tweet_tok"] = data["text_cleaned_tweet_tok"].apply(literal_eval)
        data["text_cleaned_lem"] = data["text_cleaned_lem"].apply(literal_eval)
        data["ents_tok"] = data["text_cleaned_lem"].apply(get_ents_from_tokens)

        for index, row in data.iterrows():
            tweet, created = Tweet.objects.get_or_create(index=index, text=row['text'], text_cleaned=row['text_cleaned'], afinn=row['afinn'], sentiment140=row['sentiment140'])
            if created:
                for token in row['text_cleaned_punc_tok']:
                    pt, created = PuncToken.objects.get_or_create(token=token)
                    ptr, created = PuncTokenRelation.objects.get_or_create(tweet=tweet, token=pt)
                    if not created:
                        ptr.counter += 1
                        ptr.save()
                for token in row['text_cleaned_simple_tok']:
                    st, created = SimpleToken.objects.get_or_create(token=token)
                    stre, created = SimpleTokenRelation.objects.get_or_create(tweet=tweet, token=st)
                    if not created:
                        stre.counter += 1
                        stre.save()
                for token in row['text_cleaned_tweet_tok']:
                    tt, created = TweetToken.objects.get_or_create(token=token)
                    ttr, created = TweetTokenRelation.objects.get_or_create(tweet=tweet, token=tt)
                    if not created:
                        ttr.counter += 1
                        ttr.save()
                for token in row['text_cleaned_stop_punc']:
                    swt, created = StopwordToken.objects.get_or_create(token=token)
                    swtr, created = StopwordTokenRelation.objects.get_or_create(tweet=tweet, token=swt)
                    if not created:
                        swtr.counter += 1
                        swtr.save()
                for token in row['text_cleaned_lanc_stem']:
                    ls, created = LancasterStem.objects.get_or_create(token=token)
                    lsr, created = LancasterStemRelation.objects.get_or_create(tweet=tweet, token=ls)
                    if not created:
                        lsr.counter += 1
                        lsr.save()
                for token in row['text_cleaned_port_stem']:
                    ps, created = PorterStem.objects.get_or_create(token=token)
                    psr, created = PorterStemRelation.objects.get_or_create(tweet=tweet, token=ps)
                    if not created:
                        psr.counter += 1
                        psr.save()
                for token in row['text_cleaned_snow_stem']:
                    ss, created = SnowballStem.objects.get_or_create(token=token)
                    ssr, created = SnowballStemRelation.objects.get_or_create(tweet=tweet, token=ss)
                    if not created:
                        ssr.counter += 1
                        ssr.save()
                for token in row['text_cleaned_lem']:
                    lms, created = LemmatizerStem.objects.get_or_create(token=token)
                    lmsr, created = LemmatizerStemRelation.objects.get_or_create(tweet=tweet, token=lms)
                    if not created:
                        lmsr.counter += 1
                        lmsr.save()
                for entity in row['ents_tok'].ents:
                    et, created = Entity.objects.get_or_create(entity=entity.text.lower(), label=entity.label_)
                    etr, created = EntityRelation.objects.get_or_create(tweet=tweet, entity=et)
                    if not created:
                        etr.counter += 1
                        etr.save()
        return context
