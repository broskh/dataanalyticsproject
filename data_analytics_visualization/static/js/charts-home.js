/*global $, document, Chart, LINECHART, data, options, window*/
$(document).ready(function () {

    'use strict';

    // Main Template Color
    // const brandPrimary = '#33b35a';


    // // ------------------------------------------------------- //
    // // Line Chart
    // // ------------------------------------------------------ //
    // let LINECHART = $('#lineCahrt');
    // let myLineChart = new Chart(LINECHART, {
    //     type: 'line',
    //     options: {
    //         legend: {
    //             display: false
    //         }
    //     },
    //     data: {
    //         labels: ["Jan", "Feb", "Mar", "Apr", "May", "June", "July"],
    //         datasets: [
    //             {
    //                 label: "My First dataset",
    //                 fill: true,
    //                 lineTension: 0.3,
    //                 backgroundColor: "rgba(77, 193, 75, 0.4)",
    //                 borderColor: brandPrimary,
    //                 borderCapStyle: 'butt',
    //                 borderDash: [],
    //                 borderDashOffset: 0.0,
    //                 borderJoinStyle: 'miter',
    //                 borderWidth: 1,
    //                 pointBorderColor: brandPrimary,
    //                 pointBackgroundColor: "#fff",
    //                 pointBorderWidth: 1,
    //                 pointHoverRadius: 5,
    //                 pointHoverBackgroundColor: brandPrimary,
    //                 pointHoverBorderColor: "rgba(220,220,220,1)",
    //                 pointHoverBorderWidth: 2,
    //                 pointRadius: 1,
    //                 pointHitRadius: 0,
    //                 data: [50, 20, 60, 31, 52, 22, 40],
    //                 spanGaps: false
    //             },
    //             {
    //                 label: "My First dataset",
    //                 fill: true,
    //                 lineTension: 0.3,
    //                 backgroundColor: "rgba(75,192,192,0.4)",
    //                 borderColor: "rgba(75,192,192,1)",
    //                 borderCapStyle: 'butt',
    //                 borderDash: [],
    //                 borderDashOffset: 0.0,
    //                 borderJoinStyle: 'miter',
    //                 borderWidth: 1,
    //                 pointBorderColor: "rgba(75,192,192,1)",
    //                 pointBackgroundColor: "#fff",
    //                 pointBorderWidth: 1,
    //                 pointHoverRadius: 5,
    //                 pointHoverBackgroundColor: "rgba(75,192,192,1)",
    //                 pointHoverBorderColor: "rgba(220,220,220,1)",
    //                 pointHoverBorderWidth: 2,
    //                 pointRadius: 1,
    //                 pointHitRadius: 10,
    //                 data: [65, 59, 30, 81, 46, 55, 30],
    //                 spanGaps: false
    //             }
    //         ]
    //     }
    // });


    // ------------------------------------------------------- //
    // Pie Chart
    // ------------------------------------------------------ //
    let PIECHART = $('.pieChart');
    PIECHART.each(function () {
        let this_chart = $(this);
        let pc_labels = this_chart.data("labels").split(",");
        let pc_data = this_chart.data("data").split(",");
        let pc_backgroundColor = this_chart.data("background-color").split(",");
        let pc_hoverBackgroundColor = this_chart.data("hover-background-color").split(",");
        new Chart(this_chart, {
            type: 'doughnut',
            data: {
                labels: pc_labels,
                datasets: [
                    {
                        data: pc_data,
                        borderWidth: [1, 1, 1],
                        backgroundColor: pc_backgroundColor,
                        hoverBackgroundColor: pc_hoverBackgroundColor
                    }]
            }
        });
    });


    // ------------------------------------------------------- //
    // Plot Chart
    // ------------------------------------------------------ //
    let BARCHART = $('.barChart');
    BARCHART.each(function () {
        let this_chart = $(this);
        let bc_labels = this_chart.data("labels").split(",");
        let bc_data = this_chart.data("data").split(",");
        let bc_backgroundColor = [];
        let bc_borderColor = [];
        bc_data.forEach(function () {
            bc_backgroundColor.push(this_chart.data("background-color"));
            bc_borderColor.push(this_chart.data("border-color"));
        });
        new Chart(this_chart, {
            type: 'bar',
            data: {
                labels: bc_labels,
                datasets: [
                    {
                        label: "Frequenza",
                        backgroundColor: bc_backgroundColor,
                        borderColor: bc_borderColor,
                        borderWidth: 1,
                        data: bc_data,
                    }
                ]
            }
        });
    });

});
