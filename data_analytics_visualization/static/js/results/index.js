$(document).ready(function(){
    $("#spinner").addClass("hidden");

    $("table.no-order").DataTable();
    $("table.order-2-desc").DataTable({
        "order": [[ 1, 'desc' ]]
    });

    $("#pos_entity_select").change(function () {
        let entity_id = $(this).val();
        $.ajax({
            url: 'ajax/get-entity-details/',
            data: {
                'entity_id': entity_id
            },
            dataType: 'json',
            success: function (data) {
                $("#pos_entity_tweet").html(data.tweet);
                $("#pos_entity_nel").text(JSON.stringify(data.nel, undefined, 2));
            }
        });
    });

    $("#neg_entity_select").change(function () {
        let entity_id = $(this).val();
        $.ajax({
            url: 'ajax/get-entity-details/',
            data: {
                'entity_id': entity_id
            },
            dataType: 'json',
            success: function (data) {
                $("#neg_entity_tweet").html(data.tweet);
                $("#neg_entity_nel").text(JSON.stringify(data.nel, undefined, 2));
            }
        });
    });

    $("#other_entity_select").change(function () {
        let entity_id = $(this).val();
        $.ajax({
            url: 'ajax/get-entity-details/',
            data: {
                'entity_id': entity_id
            },
            dataType: 'json',
            success: function (data) {
                $("#other_entity_tweet").html(data.tweet);
                $("#other_entity_nel").text(JSON.stringify(data.nel, undefined, 2));
            }
        });
    });
});