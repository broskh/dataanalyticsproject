$(document).ready(function(){
    $("#spinner").addClass("hidden");

    $("#dynamic_tool").submit(function (event) {
        let text = $("#text_tool").val();
        $.ajax({
            url: 'ajax/text-tool/',
            data: {
                'text': text
            },
            dataType: 'json',
            success: function (data) {
                $(".results").removeClass("hidden");
                $("#text_text").text(text);
                $("#text_text_cleaned").text(data.text_cleaned);
                $("#text_punc_tokens").text(JSON.stringify(data.punct_tokens, undefined, 2));
                $("#text_simple_tokens").text(JSON.stringify(data.simple_tokens, undefined, 2));
                $("#text_tweet_tokens").text(JSON.stringify(data.tweet_tokens, undefined, 2));
                $("#text_stopwords_tokens").text(JSON.stringify(data.stopwords_tokens, undefined, 2));
                $("#text_lancaster_stems").text(JSON.stringify(data.lancaster_stems, undefined, 2));
                $("#text_porter_stems").text(JSON.stringify(data.porter_stems, undefined, 2));
                $("#text_snowball_stems").text(JSON.stringify(data.snowball_stems, undefined, 2));
                $("#text_lemmatizer_stems").text(JSON.stringify(data.lemmatizer_stems, undefined, 2));
                $("#text_text_lt").text(text);
                $("#text_afinn").text(data.afinn);
                $("#text_sentiment140").text(data.sentiment140);
                $("#text_entities").text(JSON.stringify(data.entities, undefined, 2));
                $("#text_entity_tweet").html(data.tagged);
                $("#text_entity_nel").text(JSON.stringify(data.nel, undefined, 2));
            }
        });
        event.preventDefault();
    });
});