from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.ToolsView.as_view(), name='index'),
    url(r'^ajax/text-tool/$', views.text_tool, name='text_tool'),
]