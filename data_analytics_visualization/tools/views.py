import string

from django.http import JsonResponse
from django.views.generic import TemplateView

from nltk import wordpunct_tokenize, TweetTokenizer, word_tokenize
from nltk.stem.lancaster import LancasterStemmer
from nltk.stem.porter import PorterStemmer
from nltk.stem.snowball import SnowballStemmer
from spacy import displacy

from data_analytics_visualization.utils import datatxt, remove_urls, chat_words_conversion, remove_html, other_stop, \
    lemmatize_words, afinn, classifier, extract_features, get_nlp_tweet_to_render, get_ents_from_tokens


class ToolsView(TemplateView):
    template_name = 'tools/index.html'

    def get_context_data(self, **kwargs):
        context = super(ToolsView, self).get_context_data(**kwargs)

        context['webpage_title'] = 'Analisi esplorativa - Tools Dinamico'
        return context


def text_tool(request):
    text = request.GET.get('text', None)
    text_cleaned = remove_urls(text)
    text_cleaned = remove_html(text_cleaned)
    text_cleaned = chat_words_conversion(text_cleaned)
    tokening = TweetTokenizer(strip_handles=True, reduce_len=True)
    tweet_tokens = tokening.tokenize(text_cleaned)
    punctuation = string.punctuation
    stopwords_removed = [item for item in tweet_tokens if item not in punctuation]
    stopwords_removed = [item for item in stopwords_removed if item.lower() not in other_stop]
    lancaster_stemmer = LancasterStemmer()
    lancaster_stems = [lancaster_stemmer.stem(item) for item in stopwords_removed]
    porter_stemmer = PorterStemmer()
    porter_stems = [porter_stemmer.stem(item) for item in stopwords_removed]
    snowball_stemmer = SnowballStemmer("english")
    snowball_stems = [snowball_stemmer.stem(item) for item in stopwords_removed]
    lemmatizer_stems = lemmatize_words(stopwords_removed)
    entities = get_ents_from_tokens(lemmatizer_stems)

    data = {
        'text_cleaned': text_cleaned,
        'simple_tokens': wordpunct_tokenize(text_cleaned),
        'punct_tokens': word_tokenize(text_cleaned),
        'tweet_tokens': tweet_tokens,
        'stopwords_tokens': stopwords_removed,
        'lancaster_stems': lancaster_stems,
        'porter_stems': porter_stems,
        'snowball_stems': snowball_stems,
        'lemmatizer_stems': lemmatizer_stems,
        'afinn': int(afinn.score(text_cleaned)),
        'sentiment140': classifier.classify(extract_features(lemmatizer_stems)),
        'entities': [item.text for item in entities.ents],
        'tagged': displacy.render(get_nlp_tweet_to_render(text, entities),
                                 style='ent', page=True, manual=True),
        'nel': datatxt.nex(text).annotations
    }
    return JsonResponse(data)
